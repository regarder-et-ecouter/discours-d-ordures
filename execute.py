#!/usr/bin/python3

# importing all necessery modules
import re
import json
import sys
from wordcloud import WordCloud, STOPWORDS
import matplotlib.pyplot as plt
import csv


def generate_word_index():
    f = open("posts.csv", "r")
    reader = csv.DictReader(f)

    stopwords = list(STOPWORDS)
    stopwords.extend(
        [
            "will",
            "us",
            "now",
            "one",
            "go",
            "going",
            "never",
            "see",
            "dont",
            "must",
            "im",
            "got",
            "may",
            "much",
            "cant",
            "two",
            "yet",
            "2",
            "3",
            "4",
            "didnt",
            "oh",
            "1",
            "6",
            "mr",
            "lets",
            "let",
            "hes",
            "lot",
            "o",
            "u",
            "e",
            "la",
            "de",
        ]
    )
    word_index = {}

    for row in reader:
        val = str(row["text"].lower())
        if "i just joined parler" in val:
            continue
        words = list(
            filter(lambda x: len(x) > 0, re.sub("[^A-z0-9'\s]", "", val).split())
        )

        for word in words:
            if word not in stopwords:
                if word_index.get(word):
                    word_index[word] += 1
                else:
                    word_index[word] = 1

    # reduce word_index to top 500
    word_counts = []
    for word, count in word_index.items():
        word_counts.append((word, count))
    word_counts = sorted(word_counts, key=lambda x: x[1], reverse=True)
    word_index = {}
    for word, count in word_counts[:500]:
        word_index[word] = count

    json.dump(word_index, open("./wordindex.json", "w"), indent=2)
    print(json.dumps(word_index, indent=2))


def generate_wordcloud():
    word_index = json.load(open("wordindex.json", "r"))
    wordcloud = WordCloud(
        background_color="white", width=1920, height=1080
    ).generate_from_frequencies(word_index)

    # plot the WordCloud image
    plt.figure(figsize=(8, 8), facecolor=None)
    plt.imshow(wordcloud)
    plt.axis("off")
    plt.tight_layout(pad=0)

    plt.show()


if __name__ == "__main__":
    if len(sys.argv) > 1:
        if sys.argv[1] == "json":
            generate_word_index()
        elif sys.argv[1] == "wordcloud":
            generate_wordcloud()
    else:
        generate_word_index()
        generate_wordcloud()
