# discours d'ordures

## regarder et écouter

### Description:

This is a script that will generate a word cloud from over 377k text posts
that were leaked during the week of the insurrection at the US capitol.

### Example:

![example](wordcloud.png "Example")

### Installation requirements:

- python3
- pip

### Install dependencies:

```bash
pip install wordcloud
# workaround for memory errors: https://github.com/amueller/word_cloud/issues/516
pip install --no-binary=Pillow Pillow==6.2.2 --force
```

### Download posts

https://docs.google.com/spreadsheets/d/1A-rSBbreZEAHsqKiGOKLu6dIbGt3g943ETJFPn7CI_I

Download as CSV file (named posts.csv in this directory)

### Usage:

```bash
python3 execute.py
```

### Development:

Pull requests welcome!
